const { Touchscreen } = require("puppeteer");

module.exports = {
  env: {
    browser: true,
    node: true,
    commonjs: true,
    es2021: true
  },
  globals: {
    process: true
  },
  extends: 'eslint:recommended',
  parserOptions: {
    ecmaVersion: 12
  },
  rules: {
    quotes: [1, 'single'],
    semi: [1, 'never']
  }
}
